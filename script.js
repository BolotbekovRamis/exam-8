'use strict';

$("form").submit(function(e) {
    e.preventDefault();
        $("#display");
    let name =  e.currentTarget.name.value;
    if(!name || name.length <2){
    
    alert("Incorrect name or such a country does not exist! Try again!");
    document.getElementById("country").value='';
    $("#country").focus();

    return;
    }
        getCountryName(name)
        .then(result =>{
            result.forEach(element => {
                let card = $('<div>', {class: "card"}).appendTo('#display');
                let country = $('<div>', {class: "country-info"}).appendTo(card);
                let img = $('<div>', {class: "img"}).appendTo(country);
                $('<img>', {src: element.flag}).appendTo(img);
                let text = $('<div>', {class: "right-text"}).appendTo(country);
                $('<p>', {text: "Name: " + element.name, class: "info"}).appendTo(text);
                $('<p>', {text: "Capital: " + element.capital, class: "info"}).appendTo(text);
                element.currencies.forEach(element =>{
                    let currency = $('<div>', {text: 'Currency: ',
                        class: "currencies"
                    }).appendTo(text);
                    $('<span>', {text: element.code + " "}).appendTo(currency);
                });
                $('<p>', {text: "Region: " + element.region, class: "info"}).appendTo(text);
                let infos = $('<div>', {class: "right"}).appendTo(country);
                let name =  e.currentTarget.name.value;
                $(`<a href=https://www.google.com/search?q=${name} target="_blank">More information</a>`).appendTo(text);
            });
        })
        .catch(err =>console.log(err));
    });

    async function getCountryName(name){
        const res = await fetch(`https://restcountries.eu/rest/v2/name/${name}`);
        const resData = await res.json();
        return resData;
    }